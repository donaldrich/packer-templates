```sh
root@mysystem:~/# parted myimage.img
GNU Parted 2.3
Using /root/myimage.img
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) u
Unit?  [compact]? B
(parted) print
Model:  (file)

Number  Start       End          Size         Type     File system  Flags
 1      512B        128000511B   128000000B   primary  fat16        lba
 2      128000512B  6999000063B  6870999552B  primary  ext4

❯ sudo fdisk -l kali-64.img
Disk kali-64.img: 6.5 GiB, 6999999488 bytes, 13671874 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xf4fec10a

Device       Boot  Start      End  Sectors   Size Id Type
kali-64.img1           1   250000   250000 122.1M  c W95 FAT32 (LBA)
kali-64.img2      250001 13669921 13419921   6.4G 83 Linux

cat << EOF > "${basedir}"/kali-${architecture}/etc/fstab
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
proc            /proc           proc    defaults          0       0
/dev/mmcblk0p1  /boot           vfat    defaults          0       2
/dev/mmcblk0p2  /               ext4    defaults,noatime  0       1
EOF

```
