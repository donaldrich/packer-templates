source "arm-image" "raspbian" {
  iso_url           = "http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2020-02-14/2020-02-13-raspbian-buster-lite.zip"
  iso_checksum_type = "sha256"
  iso_checksum      = "12ae6e17bf95b6ba83beca61e7394e7411b45eba7e6a520f434b0748ea7370e8"
  target_image_size = 4294967296
}

build {
  sources = ["source.arm-image.raspbian"]

  provisioner "shell" {
    inline = ["touch /boot/ssh"]
  }

  // provisioner "shell" {
  //   inline = ["mkdir var.image_home_dir/.ssh"]
  // }

  // provisioner "file" {
  //   source = "var.ssh_key_src/.ssh/id_ed25519.pub"
  //   destination = "var.image_home_dir/.ssh/authorized_keys"
  // }

  // provisioner "shell" {
  //   inline = [
  //     "echo 'network={' >> /etc/wpa_supplicant/wpa_supplicant.conf",
  //     "echo '    ssid=\"{{user `wifi_name`}}\"' >> /etc/wpa_supplicant/wpa_supplicant.conf",
  //     "echo '    psk=\"{{user `wifi_password`}}\"' >> /etc/wpa_supplicant/wpa_supplicant.conf",
  //     "echo '}' >> /etc/wpa_supplicant/wpa_supplicant.conf"
  //   ]
  // }

  provisioner "shell" {
    inline = [
      "sudo chown -R pi:pi /home/pi",
      "apt-get update",
      "curl -sSL https://get.docker.com | sh",
      "apt-get update",
      "apt-get upgrade -yqq",
      "apt-get install -yqq curl",
      "sudo usermod -aG docker pi"
    ]
  }

  post-processor "compress" {
    output            = "/build/zip/raspbian.img.zip"
    compression_level = 9
  }
}