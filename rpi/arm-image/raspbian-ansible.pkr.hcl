source "arm-image" "raspbian" {
  iso_url           = "http://downloads.raspberrypi.org/raspbian_lite/images/raspbian_lite-2020-02-14/2020-02-13-raspbian-buster-lite.zip"
  iso_checksum_type = "sha256"
  iso_checksum      = "12ae6e17bf95b6ba83beca61e7394e7411b45eba7e6a520f434b0748ea7370e8"
  mount_path        = "/build/mnt/raspbian"
}

# A build starts sources and runs provisioning steps on those sources.
build {
  sources = ["source.arm-image.raspbian"]

  // provisioner "shell" {
  //   inline = [
  //     "apt-get install -qqy ansible"
  //   ]
  // }

  provisioner "shell" {
    inline = [
      "sudo -E ANSIBLE_FORCE_COLOR=1 PYTHONUNBUFFERED=1 ansible-playbook -i '/build/mnt/raspbian', -v -c chroot /build/rasbian.yml"
    ]
  }

  // provisioner "ansible-local" {
  //   playbook_file = "local.yml"
  //   inventory_directory = "{{ user `img_mount_path` }}"
  // }

}