source "arm-image" "kali-pi-64" {
  iso_url           = "https://images.offensive-security.com/arm-images/kali-linux-2020.1a-rpi3-nexmon-64.img.xz"
  iso_checksum_type = "sha256"
  iso_checksum      = "40d7ceed8f421069b00858ddb0810a47d13c54bab4fb2de6471d91bd8e5daa8d"
}

# A build starts sources and runs provisioning steps on those sources.
build {
  sources = [
    "source.arm-image.kali-pi-64"
  ]

  provisioner "shell" {
    inline = [
      "apt-mark hold kali-themes kali-desktop-core kali-desktop-xfce"
    ]
  }

  provisioner "shell" {
    inline = [
      "DEBIAN_FRONTEND=noninteractive apt-get -qq update",
      "DEBIAN_FRONTEND=noninteractive apt-get -qqy dist-upgrade"
    ]
  }

  post-processor "compress" {
    output            = "/build/zip/kali-arm64-rpi.img.zip"
    compression_level = 9
  }
}