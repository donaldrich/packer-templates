variable "storage_pool" {
  type    = string
  default = "local-lvm"
}

   # iso_checksum             = "sha256:fbbb3b86567892f91b8298be7c03e9be8c78c6f048e4c6fff539948743465d79"
    # iso_storage_pool         = "local"
    # iso_url                  = "https://cdimage.kali.org/kali-2020.4/kali-linux-2020.4-installer-netinst-amd64.iso"
    # iso_checksum             = "sha256:50492d761e400c2b5e22c8f253dd6f75c27e4bc84e33c2eff272476a0588fb02"
    # iso_storage_pool         = "local"
    # iso_url                  = "https://cdimage.kali.org/kali-2020.4/kali-linux-2020.4-installer-amd64.iso"


source "proxmox" "kali" {
  cores        = "2"
  cpu_type= "host"
  os = "l26"
  disks {
    disk_size         = "16G"
    storage_pool      = var.storage_pool
    storage_pool_type = "lvm"
    type              = "scsi"
  }
  http_directory           = "http"
  insecure_skip_tls_verify = true
  memory                   = 2048
  qemu_agent = true
  # scsi_controller      = "virtio-scsi-pci"
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  unmount_iso   = true
  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")

  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "90m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
  # ssh_password  = "kali"
  # ssh_timeout   = "60m"
  # ssh_username  = "kali"
}


build {
  name = "kali"
  description = "kali"
  source "source.proxmox.kali" {
    vm_id = "407"
    template_name = "template-kali"
    iso_file = "local:iso/ubuntu-20.04.1-desktop-amd64.iso"
    boot_command = [
      "<esc><esc><f6><esc><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
      " auto=true priority=critical noprompt ",
      " automatic-ubiquity ",
      " url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/desktop/preseed.cfg ",
      " keyboard-configuration/layoutcode=us ",
      " ubiquity/reboot=true ",
      " languagechooser/language-name=English ",
      " countrychooser/shortlist=IN ",
      " localechooser/supported-locales=en_US.UTF-8 ",
      " debian-installer/locale=en_US ",
      " netcfg/choose_interface=auto ",
      " boot=casper ",
      " initrd=/casper/initrd ",
      " quiet splash noprompt noshell ",
      " --- <wait>",
      "<enter><wait>"
        # "<esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc>",
        # " /install.amd/vmlinuz<wait>",
        # " auto<wait>",
        # " console-setup/ask_detect=false<wait>",
        # " console-setup/layoutcode=us<wait>",
        # " console-setup/modelcode=pc105<wait>",
        # " debconf/frontend=noninteractive<wait>",
        # " debian-installer=en_US<wait>",
        # " fb=false<wait>",
        # " initrd=/install.amd/initrd.gz<wait>",
        # " kbd-chooser/method=us<wait>",
        # " netcfg/choose_interface=eth0<wait>",
        # " console-keymaps-at/keymap=us<wait>",
        # " keyboard-configuration/xkb-keymap=us<wait>",
        # " keyboard-configuration/layout=USA<wait>",
        # " keyboard-configuration/variant=USA<wait>",
        # " locale=en_US<wait>",
        # # " netcfg/get_domain=vm<wait>",
        # # " netcfg/get_hostname=kali<wait>",
        # " noapic<wait>",
        # " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/kali/preseed.cfg auto=true priority=critical",
        # " -- <wait>",
        # " <enter><wait>"
    ]

  }
  provisioner "shell" {
      inline = [
      "qm set 407 --scsihw virtio-scsi-pci",
      "qm set 407 --ide2 ${var.storage_pool}:cloudinit",
      "qm set 407 --boot c --bootdisk scsi0"
      # "qm set 500 --ciuser     ${var.ssh_username}",
      # "qm set 500 --cipassword ${var.ssh_password}",
      # "qm set 500 --vga std"
      ]
  pause_before = "10m"
  }
}
