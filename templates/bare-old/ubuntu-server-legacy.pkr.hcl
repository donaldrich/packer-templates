build {
  name = "ubuntu-server-legacy"
  description = "ubuntu-server-legacy"
  source "source.proxmox.debian10" {
    vm_id = "606"
    template_name = "template-ubuntu-server"
    boot_wait = "5s"
    qemu_agent = true
    cloud_init = true
    iso_file = "local:iso/ubuntu-20.04.1-legacy-server-amd64.iso"
  # iso_checksum = "f11bda2f2caed8f420802b59f382c25160b114ccc665dbac9c5046e7fceaced2"
  # iso_url =  "http://cdimage.ubuntu.com/ubuntu-legacy-server/releases/20.04.1/release/ubuntu-20.04.1-legacy-server-amd64.iso"

    boot_command = [
      "<esc><wait>",
      "<esc><wait>",
      "<enter><wait>",
      "/install/vmlinuz initrd=/install/initrd.gz",
      " auto=true priority=critical",
      " console-setup/ask_detect=false<wait>",
      " console-setup/layoutcode=us<wait>",
      " console-setup/modelcode=pc105<wait>",
      " debconf/frontend=noninteractive<wait>",
      " debian-installer=en_US.UTF-8<wait>",
      " fb=false<wait>",
      " kbd-chooser/method=us<wait>",
      " keyboard-configuration/layout=USA<wait>",
      " keyboard-configuration/variant=USA<wait>",
      " locale=en_US.UTF-8<wait>",
      " url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/legacy/preseed.cfg",
      "<enter>"
    ]
  }
  # provisioner "shell-local" {
  # environment_vars = ["DEBIAN_FRONTEND=noninteractive"]
  # inline = [
  #   "sudo apt-get update",
  #   "sudo apt-get -y upgrade",
  #   "sudo apt-get -y dist-upgrade",
  # ]
  # pause_before = "15m"
  # }
  provisioner "shell" {
  # post-processor "shell-local" {
  inline = [
      "qm set 606 --scsihw virtio-scsi-pci",
      "qm set 606 -ide2 media=cdrom,file=none",
      "qm set 606 -delete ide2",
      # "qm set 606 --ide2 local-lvm:cloudinit",
      "qm set 606 --boot c --bootdisk scsi0",
      # "qm set 606 --serial0 socket --vga serial0",
      # "qm set 606 --ipconfig0 ip=192.168.1.201/24,gw=192.168.1.1",
      # "qm set 606 --ciuser ubuntu",
      # "qm set 606 --cipassword ubuntu",
  ]
  pause_before = "10m"
  }
}
