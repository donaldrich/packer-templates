variable "storage_pool" {
  type    = string
  default = "local-lvm"
}

# iso_file                 = "local:iso/ubuntu-20.10-desktop-amd64.iso"
# iso_url = "https://releases.ubuntu.com/20.10/ubuntu-20.10-desktop-amd64.iso"
# iso_checksum = "b45165ed3cd437b9ffad02a2aad22a4ddc69162470e2622982889ce5826f6e3d"


source "proxmox" "base" {
  cores        = "8"
  cpu_type= "host"
  os = "l26"
  disks {
    disk_size         = "16G"
    storage_pool      = var.storage_pool
    storage_pool_type = "lvm"
    type              = "scsi"
  }
  http_directory           = "http"
  insecure_skip_tls_verify = true
  memory                   = 12000
  qemu_agent = true
  # scsi_controller      = "virtio-scsi-pci"
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  unmount_iso   = true
  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")

  # ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  # ssh_timeout   = "90m"
  # ssh_username  = "root"
  # ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
  ssh_password  = "kali"
  ssh_timeout   = "60m"
  ssh_username  = "kali"
}

build {
  name = "ubuntu-desktop"
  description = "ubuntu-desktop"
  source "source.proxmox.base" {
    vm_id = "409"
    template_name = "ubuntu-desktop"
    # iso_file = "local:iso/ubuntu-20.10-desktop-amd64.iso"
    iso_url = "https://releases.ubuntu.com/20.10/ubuntu-20.10-desktop-amd64.iso"
    iso_checksum = "3ef833828009fb69d5c584f3701d6946f89fa304757b7947e792f9491caa270e"
    iso_storage_pool = "ssd_ext4_240gb"
    boot_wait = "10s"
    boot_command = [
      "<esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc><esc>",
        # "<esc><esc><esc><enter><wait>",
				# "install ",
				# "auto ",
				# "console-setup/ask_detect=false ",
				# "debconf/frontend=noninteractive ",
				# "debian-installer=en_US ",
				# "hostname=desktop ",
				# "fb=false ",
				# "grub-installer/bootdev=/dev/sda<wait> ",
				# "initrd=/install/initrd.gz ",
				# "kbd-chooser/method=us ",
				# "keyboard-configuration/modelcode=SKIP ",
				# "locale=en_US ",
				# "noapic ",
				# "passwd/username=ubuntu ",
				# "passwd/user-fullname=ubuntu ",
				# "passwd/user-password=ubuntu ",
				# "passwd/user-password-again=ubuntu ",
				# "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/desktop/preseed.cfg ",
				# "-- <enter>",
        # "auto url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/desktop/preseed.cfg<enter>"
    ]
  }
}
# build {
#   description = "Debian 10"
#   sources = ["source.proxmox.debian10"]
#   provisioner "shell" {
#       inline = [
#       "qm set ${var.vm_id} --scsihw virtio-scsi-pci",
#       "qm set ${var.vm_id} --ide2 ${var.storage_pool}:cloudinit",
#       "qm set ${var.vm_id} --boot c --bootdisk scsi0",
#       # "qm set 500 --ciuser     ${var.ssh_username}",
#       # "qm set 500 --cipassword ${var.ssh_password}",
#       # "qm set 500 --vga std"
#       ]
#   pause_before = "15m"
#   }

# }
