variable "storage_pool" {
  type    = string
  default = "local-lvm"
}

source "proxmox" "debian10" {
  cores        = "2"
  cpu_type= "host"
  os = "l26"
  disks {
    disk_size         = "24G"
    storage_pool      = var.storage_pool
    storage_pool_type = "lvm"
    type              = "scsi"
  }
  http_directory           = "http"
  insecure_skip_tls_verify = true
  memory                   = 2048
  # scsi_controller      = "virtio-scsi-pci"
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  unmount_iso   = true
  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")

  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}


build {
  name = "kali"
  description = "kali"
  source "source.proxmox.debian10" {
    vm_id = "607"
    template_name = "template-kali"
    # iso_file =               "NFS:iso/kali-linux-2020.3-installer-amd64.iso"
    iso_checksum             = "sha256:fbbb3b86567892f91b8298be7c03e9be8c78c6f048e4c6fff539948743465d79"
    iso_storage_pool         = "local"
    iso_url                  = "https://cdimage.kali.org/kali-2020.4/kali-linux-2020.4-installer-netinst-amd64.iso"


    boot_command = [
    # # # "auto",
    # # # " priority=critical",
        "<esc><wait>",
        "install <wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/kali/preseed.cfg <wait>",
        "debian-installer=en_US.UTF-8 <wait>",
        "auto <wait>",
        "locale=en_US.UTF-8 <wait>",
        "kbd-chooser/method=us <wait>",
        "keyboard-configuration/xkb-keymap=us <wait>",
        "netcfg/get_hostname=kali <wait>",
        "netcfg/get_domain= <wait>",
        "fb=false <wait>",
        "debconf/frontend=noninteractive <wait>",
        "console-setup/ask_detect=false <wait>",
        "console-keymaps-at/keymap=us <wait>",
        "grub-installer/bootdev=/dev/sda <wait>",
        "<enter><wait>"
    ]
  }
  provisioner "shell" {
      inline = [
      "qm set 607 --scsihw virtio-scsi-pci",
      # "qm set 604 --ide2 ${var.storage_pool}:cloudinit",
      "qm set 607 --boot c --bootdisk scsi0"
      # "qm set 500 --ciuser     ${var.ssh_username}",
      # "qm set 500 --cipassword ${var.ssh_password}",
      # "qm set 500 --vga std"
      ]
  pause_before = "35m"
  }
}
