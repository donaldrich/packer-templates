

variable "iso_storage_pool" {
  type    = string
  default = "local"
}

variable "storage_pool" {
  type    = string
  default = "local-lvm"
}

# locals {

# }

source "proxmox" "debian10" {
  cores        = "2"
  cpu_type= "host"
  os = "l26"
  disks {
    disk_size         = "8G"
    storage_pool      = var.storage_pool
    storage_pool_type = "lvm"
    type              = "scsi"
  }
  http_directory           = "http"
  insecure_skip_tls_verify = true
  memory                   = 2048
  # scsi_controller      = "virtio-scsi-pci"
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  unmount_iso   = true
  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")

  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}





# build {
#   name = "desktop"
#   description = "desktop"
#   source "source.proxmox.debian10" {
#     vm_id = "602"
#     template_name = "template-desktop"
#     iso_file =               "local:iso/ubuntu-20.04.1-desktop-amd64.iso"
#     # iso_file                 = "local:iso/ubuntu-20.04.1-desktop-amd64.iso"

# # iso_url = "https://releases.ubuntu.com/20.04/ubuntu-20.04.1-desktop-amd64.iso"
# # iso_checksum = "b45165ed3cd437b9ffad02a2aad22a4ddc69162470e2622982889ce5826f6e3d"s

#     boot_command = [
#     "<esc><wait>",
#     "<esc><wait>",
#     "<enter><wait>",
#     "auto url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/desktop/preseed.cfg<enter>"
#     ]
#   }
#   provisioner "shell" {
#       inline = [
#       "qm set ${var.vm_id} --scsihw virtio-scsi-pci",
#       "qm set ${var.vm_id} --ide2 ${var.storage_pool}:cloudinit",
#       "qm set ${var.vm_id} --boot c --bootdisk scsi0",
#       # "qm set 500 --ciuser     ${var.ssh_username}",
#       # "qm set 500 --cipassword ${var.ssh_password}",
#       # "qm set 500 --vga std"
#       ]
#   pause_before = "15m"
#   }
# }

# build {
#   name = "ubuntu-server-live"
#   description = "ubuntu-server-live"
#   source "source.proxmox.debian10" {
#     vm_id = "606"
#     template_name = "template-ubuntu-server-live"
#     iso_file                 = "local:iso/ubuntu-20.04.1-live-server-amd64.iso"
#     boot_wait = "5s"
#   # iso_url = "https://releases.ubuntu.com/20.04/ubuntu-20.04.1-desktop-amd64.iso"
#   # iso_checksum = "b45165ed3cd437b9ffad02a2aad22a4ddc69162470e2622982889ce5826f6e3d"s
    # boot_command = [
    # "<esc><esc><esc><enter><wait>",
    # "/casper/vmlinuz root=/dev/sr0 initrd=/casper/initrd autoinstall ",
    # "ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ubuntu-live/",
    # "<enter>"
    # ]

#     boot_command = [
#       "<esc><wait><esc><wait><f6><wait><esc><wait>",
#       "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#       "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#       "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#       "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#       "<bs><bs><bs><bs><bs><bs><bs><bs>",
#       "autoinstall ds=nocloud-net;seedfrom=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ubuntu-live-server",
#       "<enter><wait>"
#       # "<esc>",
#       # "<enter><wait>",
#       # "/install/vmlinuz initrd=/install/initrd.gz",
#       # " auto=true priority=critical",
#       # " url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ubuntu-live-server",
#       # "<enter>"
#     ]
#   }
#   provisioner "shell" {
#   inline = [
#       "qm set 606 --scsihw virtio-scsi-pci",
#       "qm set 606 --ide2 ${var.storage_pool}:cloudinit",
#       "qm set 606 --boot c --bootdisk scsi0"
#   ]
#   pause_before = "15m"
#   }
# }


      # "qm set 500 --ciuser     ${var.ssh_username}",
      # "qm set 500 --cipassword ${var.ssh_password}",
      # "qm set 500 --vga std"
