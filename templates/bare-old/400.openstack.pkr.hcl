# https://docs.openstack.org/image-guide/obtain-images.html

source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  name = "focal"
  sources = ["source.null.thinkbox"]
  provisioner "shell" {
    inline = [
    "cd /tmp",
    "wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img",
    "mv focal-server-cloudimg-amd64.img focal-server-cloudimg-amd64.qcow2",
    "qm stop 401",
    "qm destroy 401",
    "qm create 401 --name 'bare-focal' --cpu cputype=host --sockets 1 --cores 4 --memory 2048 --net0 virtio,bridge=vmbr0",
    "qm importdisk 401 focal-server-cloudimg-amd64.qcow2 local-lvm",
    "qm set 401 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-401-disk-0",
    # "qm set 401 --scsihw virtio-scsi-pci --virtio0 local-lvm:vm-401-disk-0",
    "qm set 401 --ide2 local-lvm:cloudinit --boot c --bootdisk virtio0 --serial0 socket",
    "qm template 401"
    ]
  }
}

build {
  name = "centos-8"
  sources = ["source.null.thinkbox"]
  provisioner "shell" {
    inline = [
    "cd /tmp",
    "wget https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2",
    "qm stop 411",
    "qm destroy 411",
    "qm create 411 --name 'bare-centos' --cpu cputype=host --sockets 1 --cores 4 --memory 2048 --net0 virtio,bridge=vmbr0",
    "qm importdisk 411 CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2 local-lvm",
    "qm set 411 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-411-disk-0",
    # "qm set 411 --scsihw virtio-scsi-pci --virtio0 local-lvm:vm-411-disk-0",
    "qm set 411 --ide2 local-lvm:cloudinit --boot c --bootdisk virtio0 --serial0 socket",
    "qm template 411"
    ]
  }
}

build {
  name = "debian"
  sources = ["source.null.thinkbox"]
  provisioner "shell" {
    inline = [
    "cd /tmp",
    "wget http://cdimage.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2",
    "qm stop 421",
    "qm destroy 421",
    "qm create 421 --name 'bare-debian' --cpu cputype=host --sockets 1 --cores 4 --memory 2048 --net0 virtio,bridge=vmbr0",
    "qm importdisk 421 debian-10-openstack-amd64.qcow2 local-lvm",
    "qm set 421 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-421-disk-0",
    # "qm set 411 --scsihw virtio-scsi-pci --virtio0 local-lvm:vm-421-disk-0",
    "qm set 421 --ide2 local-lvm:cloudinit --boot c --bootdisk virtio0 --serial0 socket",
    "qm template 421"
    ]
  }
}

# build {
#   name = "rhel"
#   sources = ["source.null.thinkbox"]
#   provisioner "shell" {
#     inline = [
#     "cd /tmp",
#     "wget https://access.cdn.redhat.com/content/origin/files/sha256/60/604afdcce495ac09b60fbde912523763af5a041d64b34d7e32bff876eff40f28/rhel-server-7.9-x86_64-kvm.qcow2?user=f01e3e937e7a4344892c6f8f257cdce3&_auth_=1606541688_371063631cc9f75bbbf319ff6fc8c46b",
#     "qm stop 431",
#     "qm destroy 431",
#     "qm create 431 --name 'bare-rhel' --cpu cputype=host --sockets 1 --cores 4 --memory 2048 --net0 virtio,bridge=vmbr0",
#     "qm importdisk 431 rhel-server-7.9-x86_64-kvm.qcow2 local-lvm",
#     "qm set 431 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-431-disk-0",
#     # "qm set 411 --scsihw virtio-scsi-pci --virtio0 local-lvm:vm-421-disk-0",
#     "qm set 431 --ide2 local-lvm:cloudinit --boot c --bootdisk virtio0 --serial0 socket",
#     "qm template 431"
#     ]
#   }
# }
