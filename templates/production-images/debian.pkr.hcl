locals {
    foo = vault("/kv/credentials", "DOCKERHUB_USER")
}

source "proxmox" "debian10" {
  boot_command = [
    "<esc><wait>",
    "<esc><wait>",
    "<enter><wait>",
    "auto url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<enter>"
  ]
  boot_wait = "7s"
  cores        = "2"
  disks {
    disk_size         = "8G"
    storage_pool      = var.storage_pool
    storage_pool_type = "lvm"
    type              = "scsi"
  }
  http_directory           = "http"
  insecure_skip_tls_verify = true
  iso_file =               "local:iso/ubuntu-20.04.1-desktop-amd64.iso"
  # iso_checksum             = "sha256:2af8f43d4a7ab852151a7f630ba596572213e17d3579400b5648eba4cc974ed0"
  # iso_storage_pool         = "local"
  # iso_url                  = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.6.0-amd64-netinst.iso"
  memory                   = 2048
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }

  template_name = "template-ubuntu-desktop"
  unmount_iso   = true
  vm_id         = var.vm_id

  node          = var.proxmox_node
  password      = var.proxmox_password
  username      = var.proxmox_username
  proxmox_url   = "https://${var.proxmox_host}:8006/api2/json"
  ssh_password  = var.ssh_password
  ssh_timeout   = "5m"
  ssh_username  = var.ssh_username
  ssh_host      = var.ssh_host

  # vm_name       = "debian"
}

build {
  description = "Debian 10"
  sources = ["source.proxmox.debian10"]
  provisioner "shell" {
      inline = [
      "qm set ${var.vm_id} --scsihw virtio-scsi-pci",
      "qm set ${var.vm_id} --ide2 ${var.storage_pool}:cloudinit",
      "qm set ${var.vm_id} --boot c --bootdisk scsi0",
      # "qm set 500 --ciuser     ${var.ssh_username}",
      # "qm set 500 --cipassword ${var.ssh_password}",
      # "qm set 500 --vga std"
      ]
  pause_before = "15m"
  }

}
