source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  name = "focal"
  sources = ["source.null.thinkbox"]
  provisioner "file" {
    source = "ubuntu-server/ubuntu-server.yml"
    destination = "/var/lib/vz/snippets/ubuntu-server.yml"
  }
  provisioner "shell" {
    inline = [
    "cd /tmp",
    "wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img",
    "mv focal-server-cloudimg-amd64.img focal-server-cloudimg-amd64.qcow2",
    "qm stop 401",
    "qm destroy 401",
    "qm create 401 --name '401-server' --cpu cputype=host --sockets 1 --cores 4 --memory 2048 --net0 virtio,bridge=vmbr0",
    "qm importdisk 401 focal-server-cloudimg-amd64.qcow2 local-lvm",
    "qm set 401 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-401-disk-0",
    "qm set 401 --scsihw virtio-scsi-pci --virtio0 local-lvm:vm-401-disk-0",
    "qm set 401 --ide2 local-lvm:cloudinit --boot c --bootdisk virtio0 --serial0 socket",
    "qm template 401"
    # "qm set 401  --vga serial0",
    # "qm set 401 ",
    # "qm set 9001 -agent 1",
    # "qm set 9001 -hotplug disk,network,usb",
    # "qm set 9001 --vga serial0",
    # "qm set 9001 --ipconfig0 ip=192.168.1.202/24,gw=192.168.1.1",
    # "qm set 9001 --ciuser packer",
    # "qm set 9001 --cipassword packer",
    # "qm set 9001 --cicustom 'user=local:snippets/ubuntu-server.yml'",
    # "qm set 9001 --sshkeys /var/lib/vz/snippets/id_rsa.pub",

    # "qm resize 9001 virtio0 +8G",

    ]
  }
}

