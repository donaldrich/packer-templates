source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  name = "ssh_pass"
  sources = ["source.null.thinkbox"]
  provisioner "file" {
    source = "ubuntu-server/ubuntu-server.yml"
    destination = "/var/lib/vz/snippets/ubuntu-server.yml"
  }
  provisioner "shell" {
    inline = [
    "qm set 9001 --ipconfig0 ip=192.168.1.202/24,gw=192.168.1.1",
    "qm set 9001 --ciuser cloudinit",
    "qm set 9001 --cipassword cloudinit",
    "qm resize 9001 virtio0 +8G"
    ]
  }
}


build {
  name = "ssh_key"
  sources = ["source.null.thinkbox"]
  provisioner "file" {
    source = "ubuntu-server/ubuntu-server.yml"
    destination = "/var/lib/vz/snippets/ubuntu-server.yml"
  }
  provisioner "shell" {
    inline = [
    "qm set 9001 --ipconfig0 ip=192.168.1.202/24,gw=192.168.1.1",
    "qm set 9001 --ciuser cloudinit",
    # "qm set 9001 --cipassword packer",
    # "qm set 9001 --cicustom 'user=local:snippets/ubuntu-server.yml'",
    "qm set 9001 --sshkeys /var/lib/vz/snippets/id_rsa.pub",
    "qm resize 9001 virtio0 +8G"
    ]
  }
}

