# This file was autogenerate by the BETA 'packer hcl2_upgrade' command. We
# recommend double checking that everything is correct before going forward. We
# also recommend treating this file as disposable. The HCL2 blocks in this
# file can be moved to other files. For example, the variable blocks could be
# moved to their own 'variables.pkr.hcl' file, etc. Those files need to be
# suffixed with '.pkr.hcl' to be visible to Packer. To use multiple files at
# once they also need to be in the same folder. 'packer inspect folder/'
# will describe to you what is in that folder.

# All generated input variables will be of string type as this how Packer JSON
# views them; you can later on change their type. Read the variables type
# constraints documentation
# https://www.packer.io/docs/from-1.5/variables#type-constraints for more info.
# "timestamp" template function replacement
locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }

# source blocks are generated from your builders; a source can be referenced in
# build blocks. A build block runs provisioner and post-processors onto a
# source. Read the documentation for source blocks here:
# https://www.packer.io/docs/from-1.5/blocks/source
#could not parse template for following block: "template: generated:23: unexpected \"\\\\\" in operand"

variable "password" {
  type        = string
  default     = "password"
  description = "description of the `foo` variable"
  sensitive   = false
}

variable "username" {
  type        = string
  default     = "root@pam"
  description = "description of the `foo` variable"
  sensitive   = false
}

variable "locale" {
  type        = string
  default     = "en_US"
  description = "description of the `foo` variable"
  sensitive   = false
}

# a build block invokes sources and runs provisionning steps on them. The
# documentation for build blocks can be found here:
# https://www.packer.io/docs/from-1.5/blocks/build
build {
  sources = ["source.proxmox.ubuntu_2004"]

}

source "proxmox" "ubuntu_2004" {
  disks {
    disk_size         = "8G"
    storage_pool      = "local-lvm"
    storage_pool_type = "lvm"
    type              = "scsi"
  }
  http_directory           = "./http"
  insecure_skip_tls_verify = true
  iso_file                 = "NFS:iso/ubuntu-20.04-live-server-amd64.iso"
  # network_adapters {
  #   bridge = "vmbr0"
  #   model = "virtio"
  # }
  node                 = "thinkbox"
  password             = var.password
  username             = var.username
  proxmox_url          = "https://192.168.1.13:8006/api2/json"
  ssh_password         = "madalynn"
  ssh_timeout          = "30m"
  scsi_controller      = "virtio-scsi-pci"
  os                   = "l26"
  memory               = 2048
  ssh_username         = "madalynn"
  template_description = "Packer Test"
  template_name        = "fedora-29"
  unmount_iso          = true
  # boot_command =  [
  #   "<esc><wait>",
  #   "<esc><wait>",
  #   "<enter><wait>",
  #   "/install/vmlinuz initrd=/install/initrd.gz",
  #   " auto=true priority=critical",
  #   " url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg",
  #   "<enter>"
  # ]
  # boot_command =  [
  #   "<esc><wait>",
  #   "<esc><wait>",
  #   "<enter><wait>",
  #   "/install/vmlinuz ",
  #   "auto ",
  #   "console-setup/ask_detect=false ",
  #   "debconf/frontend=noninteractive ",
  #   "debian-installer=en_US ",
  #   # "hostname={{ user `hostname` }} ",
  #   "fb=false ",
  #   "grub-installer/bootdev=/dev/sda<wait> ",
  #   "initrd=/install/initrd.gz ",
  #   "kbd-chooser/method=us ",
  #   "keyboard-configuration/modelcode=SKIP ",
  #   "locale=en_US ",
  #   "noapic ",
  #   "passwd/username=don ",
  #   "passwd/user-fullname=don ",
  #   "passwd/user-password=password ",
  #   "passwd/user-password-again=password ",
  #   "preseed/url=https://github.com/geerlingguy/packer-boxes/blob/master/ubuntu2004/http/preseed.cfg ",
  #   "-- <enter>"
  #   ]
  # boot_command =  [
  #   "<esc><wait>",
  #   "<esc><wait>",
  #   "<enter><wait>",
  #   "/install/vmlinuz",
  #   " auto=true",
  #   " url=https://github.com/geerlingguy/packer-boxes/blob/master/ubuntu2004/http/preseed.cfg",
  #   " locale=en_US<wait>",
  #   " console-setup/ask_detect=false<wait>",
  #   " console-setup/layoutcode=us<wait>",
  #   " console-setup/modelcode=pc105<wait>",
  #   " debconf/frontend=noninteractive<wait>",
  #   " debian-installer=en_US<wait>",
  #   " fb=false<wait>",
  #   " initrd=/install/initrd.gz<wait>",
  #   " kbd-chooser/method=us<wait>",
  #   " keyboard-configuration/layout=USA<wait>",
  #   " keyboard-configuration/variant=USA<wait>",
  #   " netcfg/get_domain=vm<wait>",
  #   " netcfg/get_hostname=vagrant<wait>",
  #   " grub-installer/bootdev=/dev/sda<wait>",
  #   " noapic<wait>",
  #   " -- <wait>",
  #   "<enter><wait>"
  # ]
  boot_command = [
    # "<enter><enter><f6><esc><wait> ",
    # "autoinstall ds=nocloud-net;seedfrom=http://{{ .HTTPIP }}:{{ .HTTPPort }}/",
    # "<enter><wait>"

    # "<esc><wait><esc><wait><f6><wait><esc><wait>",
    # "<bs><bs><bs><bs><bs>",
    # "autoinstall ds=nocloud-net;s=http://192.168.1.101:{{ .HTTPPort }}/ ",
    # "--- <enter>"
  ]
  boot_wait = "5s"
  #   boot_command = [
  #     # "<f2>",
  #     "<esc><wait>",
  #     "<esc><wait>",
  #     "<enter><wait>",
  #     "/install/vmlinuz initrd=/install/initrd.gz",
  #     "debian-installer=en_US auto locale=en_US kbd-chooser/method=us ",
  #     "fb=false debconf/frontend=noninteractive ",
  #     "keyboard-configuration/modelcode=SKIP keyboard-configuration/layout=USA ",
  #     "keyboard-configuration/variant=USA console-setup/ask_detect=false ",
  #     "partman-auto/disk=/dev/sda",
  #     "partman-auto-lvm/guided_size=max",
  #     "partman-auto/choose_recipe=atomic",
  #     "partman-auto/method=lvm",
  #     "partman-lvm/confirm=true",
  #     "partman-lvm/confirm=true",
  #     "partman-lvm/confirm_nooverwrite=true",
  #     "partman-lvm/device_remove_lvm=true",
  #     "partman/choose_partition=finish",
  #     "partman/confirm=true",
  #     "partman/confirm_nooverwrite=true",
  #     "partman/confirm_write_new_label=true",
  #     # "auto=true priority=critical ",
  #     # "passwd/user-fullname=Don ",
  #     # "passwd/username=don ",
  # #    "passwd/user-password password insecure
  # #    "passwd/user-password-again password insecure
  #     # "url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg ",
  #     "<enter>"
  #   ]

}
# boot_command = ["<esc><wait><esc><wait><f6><wait><esc><wait>", "<bs><bs><bs><bs>", "autoinstall ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ ", "--- <enter>"]
# boot_command = ["<esc><wait><esc><wait><f6><wait><esc><wait>", "<bs><bs><bs><bs><bs>", "autoinstall ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ ", "--- <enter>"]

# "<esc><wait><esc><wait><f6><wait><esc><wait>",
# "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
# "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
# "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
# "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
# "<bs><bs><bs><bs><bs><bs><bs><bs>",

# boot_command = [
#   "<esc><wait><esc><wait><f6><wait><esc><wait>",
#   "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#   "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#   "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#   "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
#   "<bs><bs><bs><bs><bs><bs><bs><bs>",
#   # "/install/vmlinuz initrd=/install/initrd.gz <wait>",
#   "/install/vmlinuz initrd=/casper/initrd <wait>",
#   " auto=true priority=critical<wait>",
#   " url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg <wait>",
#   # "--- <wait>",
#   "<enter> <wait>"
# ]


"provisioners": [
{
"type": "shell",
"inline": [
"while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done"
]
}
]
"boot_command": [
"<esc><wait><esc><wait><f6><wait><esc><wait>",
"<bs><bs><bs><bs><bs>",
"autoinstall ds=nocloud-net;s=http://192.168.1.101:{{ .HTTPPort }}/ ",
"--- <enter>"
],


      // "boot_command": [
      //   "<esc><wait><esc><wait><f6><wait><esc><wait>",
      //   "<bs><bs><bs><bs><bs>",
      //   "autoinstall ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ ",
      //   "--- <enter>"
      // ],
      // "unmount_iso": true,
      "vm_id": "{{ user `proxmox_vm_id` }}",
      // "boot_command": [
      //   "<enter><enter><f6><esc><wait> ",
      //   "autoinstall ds=nocloud-net;seedfrom=http://192.168.1.101:{{ .HTTPPort }}/",
      //   "<enter><wait>"
      // ],
      // "boot_command": [
      //   "<esc><wait><esc><wait><f6><wait><esc><wait>",
      //   "<bs><bs><bs><bs><bs>",
      //   "autoinstall ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ ",
      //   "--- <enter>"
      // ],
      // "unmount_iso": true,
      "vm_id": "{{ user `proxmox_vm_id` }}",
      // "boot_command": [
      //   "<enter><enter><f6><esc><wait> ",
      //   "autoinstall ds=nocloud-net;seedfrom=http://192.168.1.101:{{ .HTTPPort }}/",
      //   "<enter><wait>"
      // ],

        "/casper/vmlinuz ",
        "root=/dev/sda ",
        "initrd=/casper/initrd ",
      "boot_command": [
        "<esc><esc><esc>",
        "<enter><wait>",
        "autoinstall ds=nocloud-net;s=http://192.168.1.101:{{ .HTTPPort }}/",
        "<enter>"
      ],
      // "unmount_iso": true,



      // "boot_command": [
      //   "<esc><wait><esc><wait><f6><wait><esc><wait>",
      //   "install",
      //   " initrd=initrd.gz",
      //   " auto=true",
      //   " priority=critical",
      //   " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/ubuntu_20.04.preseed.cfg",
      //   " --- <wait>",
      //   "<enter><wait>"
      // ]
