source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  name = "file-test"
  description = "file-test"
  sources = [
    "source.null.thinkbox"
  ]
  provisioner "file" {
    # only = ["source.null.thinkbox"]
    source = "cloud-init/vscode-server/user-data.yml"
    destination = "/var/lib/vz/snippets/user-data.yml"
  }


  # source "source.proxmox-clone.template" {


  # }

  # provisioner "ansible" {
  #   playbook_file = "./playbook.yml"

  # }

}
