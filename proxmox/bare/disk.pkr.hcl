source "null" "proxmox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  source "source.null.proxmox" {
    # name = "604"
  }
  provisioner "file" {
    source = "user-data.yml"
    destination = "/var/lib/vz/snippets/bare.yml"
  }
  provisioner "shell" {
    inline = [
      "wget https://access.cdn.redhat.com/content/origin/files/sha256/60/604afdcce495ac09b60fbde912523763af5a041d64b34d7e32bff876eff40f28/rhel-server-7.9-x86_64-kvm.qcow2?user=f01e3e937e7a4344892c6f8f257cdce3&_auth_=1606541688_371063631cc9f75bbbf319ff6fc8c46b"
    ]
  }
}

variable "iso_link" {
  default = {
    "601" = "http://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
    "602" = "http://cdimage.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2"
    "603" = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
    "604" = "https://access.cdn.redhat.com/content/origin/files/sha256/60/604afdcce495ac09b60fbde912523763af5a041d64b34d7e32bff876eff40f28/rhel-server-7.9-x86_64-kvm.qcow2?user=f01e3e937e7a4344892c6f8f257cdce3&_auth_=1606541688_371063631cc9f75bbbf319ff6fc8c46b"
  }
}
