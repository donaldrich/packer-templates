source "proxmox" "cloud-init" {
  cores        = 2
  cpu_type= "host"
  os = "l26"
  memory = 2048
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  scsi_controller      = "virtio-scsi-pci"
  unmount_iso = true
  qemu_agent = false

  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")
  insecure_skip_tls_verify = true

  iso_url = "http://distro.ibiblio.org/tinycorelinux/11.x/x86_64/release/TinyCorePure64-current.iso"
  iso_storage_pool = "local"
  iso_checksum = "58bc33523ce10e64f56b9a9ec8a77531"

  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "90m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")

}


build {
  source "source.proxmox.cloud-init" {
    name = "601"
    vm_id = "601"
    template_name = "bare-centos"
    template_description = "bare-centos"
  }
  source "source.proxmox.cloud-init" {
    name = "602"
    vm_id = "602"
    template_name = "bare-debian"
    template_description = "bare-debian"
  }
  source "source.proxmox.cloud-init" {
    name = "603"
    vm_id = "603"
    template_name = "bare-focal"
    template_description = "bare-focal"
  }
  source "source.proxmox.cloud-init" {
    name = "604"
    vm_id = "604"
    template_name = "bare-rhel"
    template_description = "bare-rhel"
  }
  provisioner "shell" {
    inline = [
    "qm unlock ${source.name}",
    "qm stop ${source.name}",
    "wget -O /tmp/${source.name}-disk.qcow2 ${lookup(var.iso_link, source.name, "none")}"
    ]
    pause_before = "5m"
  }
}

variable "iso_link" {
  default = {
    "601" = "http://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
    "602" = "http://cdimage.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2"
    "603" = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
    "604" = "https://access.cdn.redhat.com/content/origin/files/sha256/60/604afdcce495ac09b60fbde912523763af5a041d64b34d7e32bff876eff40f28/rhel-server-7.9-x86_64-kvm.qcow2?user=f01e3e937e7a4344892c6f8f257cdce3&_auth_=1606541688_371063631cc9f75bbbf319ff6fc8c46b"
  }
}


