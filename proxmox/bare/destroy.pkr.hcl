source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}


build {
  name = "destroy_templates"
  sources = ["source.null.thinkbox"]
  provisioner "shell" {
    inline = [
    "qm unlock 601",
    "qm stop 601",
    "qm destroy 601",
    "qm unlock 602",
    "qm stop 602",
    "qm destroy 602",
    "qm unlock 603",
    "qm stop 603",
    "qm destroy 603"
    ]
  }
}



