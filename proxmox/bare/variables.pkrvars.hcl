iso_link = {
    "centos" = "http://cloud.centos.org/centos/8/x86_64/images"
    "debian" = "http://cdimage.debian.org/cdimage/openstack/current-10"
}

iso_file = {
  "centos" = "CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
  "debian" = "debian-10-openstack-amd64.qcow2"
}

template_num = {
  "centos" = 413
  "debian" = 412
}

variable "iso_file" {
  # type = map
  default = {
    "debian10" = "debian-10.6.0-amd64-netinst.iso"
  }
}

variable "iso_checksum" {
  # type = map
  default = {
    "debian10" = "sha256:2af8f43d4a7ab852151a7f630ba596572213e17d3579400b5648eba4cc974ed0"
  }
}

variable "data" {
  # type = map
  debian = {
    "link" = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.6.0-amd64-netinst.iso"
    "checksum" = "sha256:2af8f43d4a7ab852151a7f630ba596572213e17d3579400b5648eba4cc974ed0"
    "iso" = "debian-10.6.0-amd64-netinst.iso"
    boot_command = [
    "<esc><wait>",
    "auto url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/debian10/preseed.cfg<enter>"
    ]
  }
}


variable "generic_boot" {
  # type = map
  generic = {
    "{{ user `boot_command_prefix` }}",
    "/install/vmlinuz ",
    "auto ",
    "console-setup/ask_detect=false ",
    "debconf/frontend=noninteractive ",
    "debian-installer={{ user `locale` }} ",
    "hostname={{ user `hostname` }} ",
    "fb=false ",
    "grub-installer/bootdev=/dev/sda<wait> ",
    "initrd=/install/initrd.gz ",
    "kbd-chooser/method=us ",
    "keyboard-configuration/modelcode=SKIP ",
    "locale={{ user `locale` }} ",
    "noapic ",
    "passwd/username={{ user `ssh_username` }} ",
    "passwd/user-fullname={{ user `ssh_fullname` }} ",
    "passwd/user-password={{ user `ssh_password` }} ",
    "passwd/user-password-again={{ user `ssh_password` }} ",
    "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/{{ user `preseed.cfg` }} ",
    "-- <enter>"
  }
}


      # "pause_before": "20s",
      # "type": "shell",
      # "environment_vars": ["DEBIAN_FRONTEND=noninteractive"],
      # "inline": [
      #   "date > provision.txt",
      #   "sudo apt-get update",
      #   "sudo apt-get -y upgrade",
      #   "sudo apt-get -y dist-upgrade",
      #   "sudo apt-get -y install linux-generic linux-headers-generic linux-image-generic",
      #   "sudo apt-get -y install qemu-guest-agent cloud-init",
      #   "sudo apt-get -y install wget curl",
      #   "exit 0"

