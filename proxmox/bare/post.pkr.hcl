source "null" "proxmox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  source "source.null.proxmox" {
    name = "601"
  }
  source "source.null.proxmox" {
    name = "602"
  }
  source "source.null.proxmox" {
    name = "603"
  }
  # source "source.null.proxmox" {
  #   name = "604"
  # }
  provisioner "file" {
    source = "user-data.yml"
    destination = "/var/lib/vz/snippets/bare.yml"
  }
  provisioner "shell" {
    inline = [
    "qm set ${source.name} --delete ide2",
    # "qm importdisk ${source.name} /tmp/${source.name}-disk.qcow2 local-lvm",
    "qm set ${source.name} --scsi0 local-lvm:vm-${source.name}-disk-0",
    "qm set ${source.name} --boot c --bootdisk scsi0",
    "qm set ${source.name} --serial0 socket --vga serial0",
    "qm set ${source.name} --agent 1",
    "qm set ${source.name} --ide2 local-lvm:cloudinit",
    # "qm set ${source.name} --ciuser cloudinit",
    # "qm set ${source.name} --cipassword cloudinit",
    "qm set ${source.name} --ipconfig0 ip=192.168.1.111/24,gw=192.168.1.1",
    "qm set ${source.name} --cicustom 'user=local:snippets/bare.yml'",
    # "qm set 9001 --sshkeys /var/lib/vz/snippets/id_rsa.pub",

    # "qm resize 9001 virtio0 +8G",
    ]
  }
}

# build {
#   # source "source.null.proxmox" {
#   #   name = "601"
#   # }
#   # source "source.null.proxmox" {
#   #   name = "602"
#   # }
#   # source "source.null.proxmox" {
#   #   name = "603"
#   # }
#   source "source.null.proxmox" {
#     name = "604"
#   }
#   provisioner "shell" {
#     inline = [
#     # "qm set ${source.name} --delete ide2 || true",
#     # "qm importdisk ${source.name} /tmp/${source.name}-disk.qcow2 local-lvm",
#     # "qm set ${source.name} --scsi0 local-lvm:vm-${source.name}-disk-0",
#     "qm set ${source.name} --boot c --bootdisk scsi0",
#     "qm set ${source.name} --serial0 socket --vga serial0",
#     "qm set ${source.name} --agent 1",
#     # "qm set 9001 -hotplug disk,network,usb",
#     "qm set ${source.name} --ide2 local-lvm:cloudinit",
#     "qm set ${source.name} --ciuser cloudinit",
#     "qm set ${source.name} --cipassword cloudinit",
#     "qm set ${source.name} --ipconfig0 ip=192.168.1.111/24,gw=192.168.1.1"
#     # "qm set 9001 --cicustom 'user=local:snippets/ubuntu-server.yml'",
#     # "qm set 9001 --sshkeys /var/lib/vz/snippets/id_rsa.pub",

#     # "qm resize 9001 virtio0 +8G",
#     ]
#   }
# }
