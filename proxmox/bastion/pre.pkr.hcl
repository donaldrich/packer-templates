source "null" "proxmox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  source "source.null.proxmox" {
    name = "601"
  }
  provisioner "file" {
    source = "user-data.yml"
    destination = "/var/lib/vz/snippets/bastion.yml"
  }
}

