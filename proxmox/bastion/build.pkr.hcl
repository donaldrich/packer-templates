# https://github.com/cloudposse/bastion/blob/master/Dockerfile

source "null" "proxmox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

source "proxmox-clone" "bastion" {
  insecure_skip_tls_verify = true
  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")
  ssh_timeout   = "5m"
  ssh_username  = "cloudinit"
  ssh_host      = "192.168.1.111"
  ssh_password  = "cloudinit"
  scsi_controller = "virtio-scsi-pci"
  cores        = "2"
  cpu_type= "host"
  os = "l26"
  template_name = "bastion"
  template_description = "bastion"
  vm_name = "bastion"
  vm_id = "700"
  memory                   = 4096
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  clone_vm = "bare-focal"
}

build {
  name = "bastion"
  description = "bastion"
  sources = [
    "source.proxmox-clone.bastion",
    "source.null.proxmox"
  ]
  provisioner "file" {
    only = ["source.null.proxmox"]
    source = "user-data.yml"
    destination = "/var/lib/vz/snippets/bastion.yml"
  }
  # provisioner "shell" {
  #   inline = [
  #   # "qm set ${source.name} --ipconfig0 ip=192.168.1.111/24,gw=192.168.1.1",
  #   "qm set ${source.name} --cicustom 'user=local:snippets/bastion.yml'",
  #   # "qm resize 9001 virtio0 +8G",
  #   ]
  # }
  provisioner "ansible" {
    only = ["source.proxmox-clone.bastion"]
    user = "cloudinit"
    playbook_file = "ansible/bastion.yml"
    ansible_env_vars = [ "ANSIBLE_HOST_KEY_CHECKING=False", "ANSIBLE_REMOTE_TMP=/tmp" ]
    pause_before = "5m"
    roles_path = "ansible/roles"
    collections_path = "ansible/collections"
    galaxy_file = "ansible/requirements.yml"
    extra_arguments = [ "-vv" ]
  }

}
