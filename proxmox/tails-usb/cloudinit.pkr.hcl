source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

build {
  name = "focal"
  # sources = ["source.null.thinkbox"]
  # provisioner "file" {
  #   source = "cloud-init/ubuntu-server/ubuntu-server.yml"
  #   destination = "/var/lib/vz/snippets/ubuntu-server.yml"
  # }
  provisioner "shell" {
    inline = [
    "cd /mnt/pve/NFS/template/iso",
    "wget https://tails.ybti.net/tails/stable/tails-amd64-4.13/tails-amd64-4.13.img",
    "mv tails-amd64-4.13.img tails-amd64-4.13.qcow2",
    "qm stop 9002",
    "qm destroy 9002",
    "qm create 9002 --name 'template-tails-usb' --memory 2048 --net0 virtio,bridge=vmbr0 -cores 1 -sockets 1",
    "qm importdisk 9002 tails-amd64-4.13.qcow2 local-lvm",
    "qm set 9002 --scsihw virtio-scsi-pci -virtio0 local-lvm:vm-9001-disk-0",
    # "qm set 9001 --serial0 socket",
    "qm set 9002 --boot c --bootdisk virtio0",
    # "qm set 9001 -agent 1",
    "qm set 9002 -hotplug disk,network,usb",
    # "qm set 9001 --vga serial0",
    # "qm set 9001 --ipconfig0 ip=192.168.1.202/24,gw=192.168.1.1",
    # "qm set 9001 --ciuser packer",
    # "qm set 9001 --cipassword packer",
    # "qm set 9001 --cicustom 'user=local:snippets/ubuntu-server.yml'",
    # "qm set 9001 --sshkeys /var/lib/vz/snippets/id_rsa.pub",
    # "qm set 9001 --ide2 local-lvm:cloudinit",
    # "qm resize 9002 virtio0 +8G",
    "qm template 9002"
    ]
  }
}

