variable "storage_pool" {
  type    = string
  default = "local-lvm"
}

source "proxmox-clone" "template" {
  insecure_skip_tls_verify = true
  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")
  ssh_timeout   = "5m"
  ssh_username  = "ubuntu"
  ssh_host      = "192.168.1.202"
  ssh_private_key_file = "../id_rsa"
  scsi_controller = "virtio-scsi-pci"
  cores        = "2"
  cpu_type= "host"
  os = "l26"
  template_name = "minikube"
  template_description = "minikube"
  full_clone = true
  # disks {
  #   disk_size         = "20G"
  #   storage_pool      = var.storage_pool
  #   storage_pool_type = "lvm"
  #   type              = "scsi"
  # }
  vm_name = "minikube"
  vm_id = "754"
  memory                   = 4096
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  clone_vm = "template-cloud-ubuntu-server-2004"
}

build {
  name = "minikube"
  description = "minikube"
  sources = [
    "source.proxmox-clone.template"
  ]
  provisioner "ansible" {
    user = "don"
    playbook_file = "ansible/minikube.yml"
    ansible_env_vars = [ "ANSIBLE_HOST_KEY_CHECKING=False", "ANSIBLE_REMOTE_TMP=/tmp" ]
    pause_before = "10m"
  }
}
