# source "proxmox" "connection" {
#   node          = vault("/kv/credentials", "PROXMOX_NODE")
#   password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
#   username      = vault("/kv/credentials", "PROXMOX_USERNAME")
#   proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")
# }
#   cores        = "2"
#   cpu_type= "host"
#   os = "l26"
#   disks {
#     disk_size         = "16G"
#     storage_pool      = var.storage_pool
#     storage_pool_type = "lvm"
#     type              = "scsi"
#   }
#   http_directory           = "http"
#   insecure_skip_tls_verify = true
#   memory                   = 2048
#   qemu_agent = true
#   # scsi_controller      = "virtio-scsi-pci"
#   network_adapters {
#     bridge = "vmbr0"
#     model  = "virtio"
#   }
#   unmount_iso   = true


#   ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
#   ssh_timeout   = "90m"
#   ssh_username  = "root"
#   ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
#   # ssh_password  = "kali"
#   # ssh_timeout   = "60m"
#   # ssh_username  = "kali"
# }


# build {
#   name = "kali"
#   description = "kali"
#   source "source.proxmox.kali" {
#     vm_id = "404"
#     template_name = "bare-kali"
#     iso_file = "local:iso/kali-linux-2020.4-installer-amd64.iso"
#     boot_command = [
#         "<esc><wait>",
#         "/install.amd/vmlinuz<wait>",
#         " auto<wait>",
#         " console-setup/ask_detect=false<wait>",
#         " console-setup/layoutcode=us<wait>",
#         " console-setup/modelcode=pc105<wait>",
#         " debconf/frontend=noninteractive<wait>",
#         " debian-installer=en_US<wait>",
#         " fb=false<wait>",
#         " initrd=/install.amd/initrd.gz<wait>",
#         " kbd-chooser/method=us<wait>",
#         " netcfg/choose_interface=eth0<wait>",
#         " console-keymaps-at/keymap=us<wait>",
#         " keyboard-configuration/xkb-keymap=us<wait>",
#         " keyboard-configuration/layout=USA<wait>",
#         " keyboard-configuration/variant=USA<wait>",
#         " locale=en_US<wait>",
#         # " netcfg/get_domain=vm<wait>",
#         # " netcfg/get_hostname=kali<wait>",
#         " noapic<wait>",
#         " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/kali/preseed.cfg auto=true priority=critical",
#         " -- <wait>",
#         "<enter><wait>"
#     ]

#   }
#   provisioner "shell" {
#       inline = [
#       "qm set 404 --ide2 media=cdrom,file=none"
#       ]
#   pause_before = "10m"
#   }
#   provisioner "shell" {
#       inline = [
#       "qm set 404 --scsihw virtio-scsi-pci",
#       "qm set 404 --ide2 local-lvm:cloudinit",
#       "qm set 404 --boot c --bootdisk scsi0"

#       ]
#   }
# }

source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

source "proxmox" "cloud-init" {
  cores        = 2
  cpu_type= "host"
  os = "l26"
  memory = 2048
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  scsi_controller      = "virtio-scsi-pci"
  cloud_init = true
  cloud_init_storage_pool = "local-lvm"
  unmount_iso = true
  qemu_agent = true

  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")
  insecure_skip_tls_verify = true

  iso_url = "http://distro.ibiblio.org/tinycorelinux/11.x/x86_64/release/TinyCorePure64-current.iso"
  iso_storage_pool = "local"
  iso_checksum = "58bc33523ce10e64f56b9a9ec8a77531"

  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "90m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")

}


build {
  source "source.proxmox.cloud-init" {
    name = "601"
    vm_id = "601"
    template_name = "bare-centos"
    template_description = "bare-centos"
  }
  source "source.proxmox.cloud-init" {
    name = "602"
    vm_id = "602"
    template_name = "bare-debian"
    template_description = "bare-debian"
  }
  source "source.proxmox.cloud-init" {
    name = "603"
    vm_id = "603"
    template_name = "bare-focal"
    template_description = "bare-focal"
  }
  # provisioner "shell" {
  #   inline = [
  #   "qm stop ${source.name}",
  #   "qm set ${source.name} --delete ide2"
  #   ]
  #   pause_before = "30s"
  # }
  # provisioner "shell" {
  #   inline = [
  #   "qm stop ${source.name}",
  #   "cd /tmp",
  #   # "wget -O ${source.name}-disk.qcow2 ${lookup(var.iso_link, source.name, "none")}",
  #   # "qm stop 411",
  #   # "qm destroy ${lookup(var.iso_link, source.name, "none")} || true",
  #   "qm importdisk ${source.name} ${source.name}-disk.qcow2 local-lvm",
  #   "qm set ${source.name} --scsi0 local-lvm:vm-${source.name}-disk-0",
  #   "qm set ${source.name} --boot c --bootdisk scsi0",
  #   "qm set ${source.name} --serial0 socket --vga serial0" 
  #   # "qm set ${source.name} --ide2 local-lvm:cloudinit"
  #   ]
  #   pause_before = "5m"
  # }
}

variable "iso_link" {
  default = {
    "601" = "http://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.2.2004-20200611.2.x86_64.qcow2"
    "602" = "http://cdimage.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2"
    "603" = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
    "604" = "https://access.cdn.redhat.com/content/origin/files/sha256/60/604afdcce495ac09b60fbde912523763af5a041d64b34d7e32bff876eff40f28/rhel-server-7.9-x86_64-kvm.qcow2?user=f01e3e937e7a4344892c6f8f257cdce3&_auth_=1606541688_371063631cc9f75bbbf319ff6fc8c46b"
  }
}


