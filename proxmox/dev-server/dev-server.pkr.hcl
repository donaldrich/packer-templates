source "null" "thinkbox" {
  ssh_password  = vault("/kv/credentials", "PROXMOX_PASSWORD")
  ssh_timeout   = "5m"
  ssh_username  = "root"
  ssh_host      = vault("/kv/credentials", "PROXMOX_IP")
}

source "proxmox-clone" "template" {
  insecure_skip_tls_verify = true
  node          = vault("/kv/credentials", "PROXMOX_NODE")
  password      = vault("/kv/credentials", "PROXMOX_PASSWORD")
  username      = vault("/kv/credentials", "PROXMOX_USERNAME")
  proxmox_url   = vault("/kv/credentials", "PROXMOX_URL")
  ssh_timeout   = "5m"
  ssh_username  = "ubuntu"
  ssh_host      = "192.168.1.202"
  ssh_private_key_file = "./id_rsa"
  scsi_controller = "virtio-scsi-pci"
  cores        = "2"
  cpu_type= "host"
  os = "l26"
  template_name = "dev-server"
  template_description = "dev-server"
  full_clone = true
  vm_name = "dev-server"
  vm_id = "700"
  memory                   = 4096
  network_adapters {
    bridge = "vmbr0"
    model  = "virtio"
  }
  clone_vm = "template-cloud-ubuntu-server-2004"
}

build {
  name = "vscode-server"
  description = "vscode-server"
  sources = [
    # "source.null.thinkbox",
    "source.proxmox-clone.template"
  ]
  provisioner "ansible" {
    user = "don"
    playbook_file = "ansible/packer.yml"
    ansible_env_vars = [ "ANSIBLE_HOST_KEY_CHECKING=False", "ANSIBLE_REMOTE_TMP=/tmp" ]
    pause_before = "5m"
  }
  # provisioner "shell-local" {
  #   # only = ["source.null.thinkbox"]
  #   inline = [
  #     "ssh root@192.168.1.13 qm set 700 --ipconfig0 ip=192.168.1.202/24,gw=192.168.1.1",
  #     "ssh root@192.168.1.13 qm reset 700"
  #   ]
  # }
}
